#!/usr/bin/env python

import direct.directbase.DirectStart
from pandac.PandaModules import *
from direct.gui.OnscreenText import OnscreenText
import sys

GRAVITY = 0.98


class FPS(object):
    def __init__(self, keymap):
        """ create a FPS type game """
        self.keymap = keymap

        self.initCollision()
        self.loadLevel()
        self.initPlayer()

        base.accept( "escape" , sys.exit)
        base.disableMouse()
        OnscreenText(
                text="Simple FPS Movement",
                style=1,
                fg=(1,1,1,1),
                pos=(1.3,-0.95),
                align=TextNode.ARight,
                scale=.07)
        OnscreenText(
                text=__doc__,
                style=1,
                fg=(1,1,1,1),
                pos=(-1.3, 0.95),
                align=TextNode.ALeft,
                scale=.05)

    def initCollision(self):
        """ create the collision system """
        base.cTrav = CollisionTraverser()
        base.pusher = CollisionHandlerPusher()

        # Hack suggested by rdb
        base.pusher.setHorizontal(False)

        base.cTrav.showCollisions(render)

    def loadLevel(self):
        """Load the level and create colliders for all geometry.

        """
        self.level = loader.loadModel('models/testlevel.egg')
        self.level.reparentTo(render)

        # Coerce visible geometry into being collision geometry
        #self.level.setTwoSided(True)
        #for node in self.level.findAllMatches("**/+GeomNode"):
            #print "Setting IntoCollideMask on", node
            #node.node().setIntoCollideMask(BitMask32.allOn())
            #base.cTrav.addCollider(node, base.pusher)

        # Load separate octree/quadtree collision geometry from a file
        self.levelColliders = loader.loadModel('models/testlevel_colliders.bam')
        self.levelColliders.ls()
        self.levelColliders.reparentTo(render)
        for node in self.levelColliders.findAllMatches("**/+CollisionNode"):
            node.node().setFromCollideMask(BitMask32.allOff())
            node.node().setIntoCollideMask(BitMask32.allOn())

            # Show collision geometry so we can see it even when we aren't colliding
            #node.setRenderModeWireframe()
            #node.show()

    def initPlayer(self):
        """ loads the player and creates all the controls for him"""
        playerStartPos = self.level.find("**/start_point").getPos()
        playerStartPos.setX(0)
        playerStartPos.setY(3)
        playerStartPos.setZ(3)
        self.player = Player(self.keymap, playerStartPos)


class Player(object):
    """Player is the main actor in the fps game.

    """
    speed = 2
    FORWARD = Vec3(0,2,0)
    BACK = Vec3(0,-1,0)
    LEFT = Vec3(-1,0,0)
    RIGHT = Vec3(1,0,0)
    STOP = Vec3(0)
    walk = STOP
    strafe = STOP
    readyToJump = False
    jump = 0

    def __init__(self, keymap, pos):
        """Initialize the player.

        """
        self.keymap = keymap

        self.loadModel(pos)
        self.setUpCamera()
        self.createCollisions()
        self.attachControls()

        # Create mouse update, move, and jump tasks.
        taskMgr.add(self.mouseUpdate, 'mouse-task')
        taskMgr.add(self.moveUpdate, 'move-task')
        taskMgr.add(self.jumpUpdate, 'jump-task')

    def loadModel(self, pos):
        """ make the nodepath for player """
        self.node = NodePath('player')
        self.node.reparentTo(render)
        self.node.setPos(pos)
        #self.node.setScale(.2)

    def setUpCamera(self):
        """ puts camera at the players node """
        pl =  base.cam.node().getLens()
        pl.setFov(70)
        base.cam.node().setLens(pl)
        base.camera.reparentTo(self.node)

    def createCollisions(self):
        """ create a collision solid and ray for the player """
        cn = CollisionNode('player')
        cn.addSolid(CollisionSphere(0,0,0,1))
        cn.setFromCollideMask(BitMask32.bit(0))
        cn.setIntoCollideMask(BitMask32.allOff())
        solid = self.node.attachNewNode(cn)
        base.cTrav.addCollider(solid,base.pusher)
        base.pusher.addCollider(solid,self.node, base.drive.node())

        # Create collision ray.
        ray = CollisionRay()
        ray.setOrigin(0,0,-.2)
        ray.setDirection(0,0,-1)
        cn = CollisionNode('playerRay')
        cn.addSolid(ray)
        cn.setFromCollideMask(BitMask32.bit(1))
        cn.setIntoCollideMask(BitMask32.allOff())
        solid = self.node.attachNewNode(cn)
        self.nodeGroundHandler = CollisionHandlerQueue()
        base.cTrav.addCollider(solid, self.nodeGroundHandler)

    def attachControls(self):
        """ attach key events """
        for key, binding in self.keymap.iteritems():
            base.accept(key, self.__setattr__, binding)

    def mouseUpdate(self,task):
        """ this task updates the mouse """
        md = base.win.getPointer(0)
        x = md.getX()
        y = md.getY()
        if base.win.movePointer(0, base.win.getXSize()/2, base.win.getYSize()/2):
            self.node.setH(self.node.getH() -  (x - base.win.getXSize()/2)*0.1)
            base.camera.setP(base.camera.getP() - (y - base.win.getYSize()/2)*0.1)
        return task.cont

    def moveUpdate(self,task):
        """ this task makes the player move """
        # move where the keys set it
        self.node.setPos(self.node,self.walk*globalClock.getDt()*self.speed)
        self.node.setPos(self.node,self.strafe*globalClock.getDt()*self.speed)
        return task.cont

    def jumpUpdate(self,task):
        """ this task simulates gravity and makes the player jump """
        # get the highest Z from the down casting ray
        highestZ = -100
        for i in range(self.nodeGroundHandler.getNumEntries()):
            entry = self.nodeGroundHandler.getEntry(i)
            z = entry.getSurfacePoint(render).getZ()
            if z > highestZ and entry.getIntoNode().getName() == "Cube":
                highestZ = z
        # gravity effects and jumps
        self.node.setZ(self.node.getZ() + self.jump * globalClock.getDt())
        self.jump -= GRAVITY * globalClock.getDt()
        if highestZ > self.node.getZ() - .3:
            self.jump = 0
            self.node.setZ(highestZ + .3)
            if self.readyToJump:
                self.jump = 1
        return task.cont


qwertyKeymap = {
        "space": ["readyToJump", True],
        "space-up": ["readyToJump", False],
        "w": ["walk", Player.FORWARD],
        "s": ["walk", Player.BACK],
        "s-up": ["walk", Player.STOP],
        "w-up": ["walk", Player.STOP],
        "a": ["strafe", Player.LEFT],
        "d": ["strafe", Player.RIGHT],
        "a-up": ["strafe", Player.STOP],
        "d-up": ["strafe", Player.STOP],
        }

dvorakKeymap = {
        "space": ["readyToJump", True],
        "space-up": ["readyToJump", False],
        ".": ["walk", Player.FORWARD],
        "e": ["walk", Player.BACK],
        "e-up": ["walk", Player.STOP],
        ".-up": ["walk", Player.STOP],
        "o": ["strafe", Player.LEFT],
        "u": ["strafe", Player.RIGHT],
        "o-up": ["strafe", Player.STOP],
        "u-up": ["strafe", Player.STOP],
        }

if len(sys.argv) > 1 and sys.argv[1] == '-d':
    __doc__ = """Movement keys:
., e, o, u = Forward, Backward, Left, Right
space = Jump

"""
    FPS(dvorakKeymap)

else:
    __doc__ = """Movement keys:
w, s, a, d = Forward, Backward, Left, Right
space = Jump

"""
    FPS(qwertyKeymap)


run()

